# NHSUK Prototype Kit

## About the kit
The prototype kit provides a simple way to make interactive prototypes that look like pages on NHSUK. These prototypes can be used to show ideas to people you work with, and to do user research.

## Using the kit
We have tried to make it as easy as possible for everyone to use this kit, whether your a designer or developer you should be able to get the kit up and running in 10-20 mins.

## Security
Before publishing your prototypes online, they must be protected by a username and password. This will remove any confusion from members of the public finding your prototypes and mistaking them for real services.

You must protect user privacy at all times, even when using prototypes. Always make sure you are handling user data appropriately.
